#!/bin/bash


# Install updates
sudo apt-get update
sudo apg-get dist-upgrade


# Install misc system packages
sudo apt-get install -y arandr conky compton htop gmrun gtk3-nocsd \
    leafpad lubuntu-software-center mate-notification-daemon \
    mate-power-manager lxappearance network-manager-gnome nitrogen \
    pavucontrol software-properties-gtk terminator tint2 \
    ttf-ubuntu-font-family volumeicon-alsa


# Install lightdm
sudo apt-get install -y lightdm lightdm-gtk-greeter light-locker


# Install openbox packages
sudo apt-get install -y openbox obmenu openbox-menu


# Copy themes TO DO: These files should ideally be a DEB package
if [ ! -d /usr/share/icons ]; then
    sudo mkdir /usr/share/icons
fi
sudo cp -r files/icons/Moka* /usr/share/icons
echo "Copied default icon theme."

if [ ! -d /usr/share/themes/ ]; then
    sudo mkdir /usr/share/themes
fi
sudo cp -rv files/themes/pyte_dark /usr/share/themes/
sudo cp -rv files/themes/VimixDark /usr/share/themes/

if [ ! -d /usr/share/backgrounds ]; then
    sudo mkdir /usr/share/backgrounds
fi
sudo cp files/backgrounds/openbox_remix.png /usr/share/backgrounds


# Copy default openbox configuration files
if [ ! -d /etc/skel/.config/openbox ]; then
    sudo mkdir -p /etc/skel/.config/openbox
fi
sudo cp -v files/openbox/* /etc/skel/.config/openbox
chmod +x /etc/skel/.config/openbox/autostart

if [ ! -d /etc/skel/.config/tint2 ]; then
    sudo mkdir -p /etc/skel/.config/tint2
fi
sudo cp -v files/tint2/tint2rc /etc/skel/.config/tint2


# Copy conky config
sudo cp -v files/conky/conkyrc /etc/skel/.conkyrc


# Set default wallpaper
if [ ! -d /etc/skel/.config/nitrogen ]; then
    sudo mkdir -p /etc/skel/.config/nitrogen
fi
sudo cp -v files/nitrogen/*.cfg /etc/skel/.config/nitrogen/

# Copy application configuraton files
if [ ! -d /etc/skel/.config/terminator ]; then
    sudo mkdir /etc/skel/.config/terminator
fi
sudo cp -v files/terminator/config /etc/skel/.config/terminator/

# Copy lightdm config file
sudo cp -v files/lightdm/lightdm-gtk-greeter.conf /etc/lightdm/

# Copy Openbox scripts # TO DO: These shuld be a DEB package, and install to /usr/bin
# TO DO: Change menu.xml to call ob-exit from /usr/bin once the package is created
sudo cp -v files/scripts/ob-exit /usr/local/bin
sudo cp -v files/scripts/ob-welcome /usr/local/bin


# TO DO: Copy default lightdm configuration file


# Copy current configs
cp -rv /etc/skel/. /home/$USER

# All done
openbox --reconfigure
echo "Complete. Please reboot the system to finalize the changes."
echo
