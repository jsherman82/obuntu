# obuntu

obuntu is an Ubuntu-based Linux distribution featuring the Openbox window manager. Please note that this is currently pre-alpha, and is under development. It most likely doesn't work yet for its designed purpose, but volunteers are welcome. It's current state is simply an install script, but will become an installable ISO once the project is far enough along.

## Goals
* Fast
* Light
* Efficient

## Features
* Latest Ubuntu base
* Custom theme
* No default applications

## How to install
Currently, there are no ISO images available (these will be created at a later date). For now, you will need to run the obuntu_install.sh script on an existing Ubuntu installation. Simply clone this repository, then run the script. To do this, first install any flavor of Ubuntu. For best results, install from Ubuntu Minimal - it doesn't have any default applications. Then, install git and clone this repo. Finally, run the install script and reboot.

## No default applications?
Most Linux distributions contain many default applications, such as a browser, image viewer, video player, and so on. obuntu doesn't include any of those things, with the sole exceptions being a terminal emulator, and utilities to manage your screen resolution. Reason being, not all users need all of the default applications, so we include as few as possible. It's better to let the user decide which applications he or she wants on their system. Therefore, Obuntu will display a welcome screen at first boot, that will give the user the opportunity to install whatever applications they want. This way, the only applications that are installed are the ones you actually intend on using. There's certainly nothing wrong with a GNOME or KDE desktop, but Obuntu is for users that don't want the extra overhead for whatever reason.

## Exceptions to the "no default applications" rule
There are a few exceptions to the rule against default applications. A list of exceptions, and their reasoning, are below.

### Terminal emulator
Without a terminal emulator, it wouldn't be simple to install any applications. You could, of course, switch to a TTY, but having a terminal emulator installed uses very little space, and serves to also launch the post-installation menu. You can easily install a different terminal emulator and remove the one that Obuntu comes with.

### Power management, and display features
Openbox, by virtue of it being a window manager and not a desktop environment, doesn't include utilities to manage your screen resolution, battery, etc. Obuntu comes excludes utilities that manage the environment from the no-app rule. However, it opts to use as few of these as possible.

## Why use obuntu?
The goal behind Obuntu is speed and efficiency. Your CPU shouldn't be taxed by expensive file indexing services, memory-eating processes, etc. As much of your CPU should be available for your applications as possible. It should run extremely fast in a VM (virtual machines are known to be slower than hardware installations) but it should also be a good choice for your 10-year old computer, or even your brand-new i7 gaming rig. Whatever your type of computer, your CPU and memory should be as free as possible to handle the tasks you seek to accomplish.

Please note: Do NOT run this script on a production system, or any other system that contains data you care about. Until a final (stable) version is announced, this is to be considered alpha software.

## Why have the panel on the left side?
Most users nowadays have wide-screen displays. This means that you have more real-estate horizontally than you do vertically. Therefore, the panel being on the left edge takes up less screen real-estate for the majority of users. You can, of course, switch the dock to a different edge.

## What's needed
Volunteers are welcome! The following things are things I am working on, and need to be completed before this project can be considered stable:

* ISO images
* Default GTK theme
* Default Openbox theme
* Default wallpaper
* LightDM theme
* Logo
* Lots and lots of testing!!!
